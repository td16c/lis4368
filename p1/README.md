# LIS4368

## Tyler Dean 

### Project 1 Requirements 

#### Deliveriables: 

 *Links to prior assignments*
 *Sever validation images*
 *Blackboard Links:* 

[A1 Link](https://bitbucket.org/td16c/lis4368/src/04aa3f54110022913ae2ea81d7bde49c9b41eb9c/a1/?at=master "A1 Link")

[A2 Link](https://bitbucket.org/td16c/lis4368/src/04aa3f54110022913ae2ea81d7bde49c9b41eb9c/a2/?at=master "A2 Link")

[A3 Link](https://bitbucket.org/td16c/lis4368/src/04aa3f54110022913ae2ea81d7bde49c9b41eb9c/a3/?at=master "A3 Link")


*Project Screenshots* 

*Validation Screenshots*: 

![Failed Validation](img/fail.jpg)

![Passed Validation](img/good.jpg)






