> **NOTE:** This README.md file should be placed at the **root of each 
of your repos directories.** 
>
>Also, this file **must** use Markdown syntax, and provide documentation
as per below--otherwise, points **will** be deducted.
>

#LIS4368 - Advanced Web Application Development 

## Tyler M. Dean 

### Assignment 1 Requirements: 

*Includes:*

1. Distributed Version Control with Git and Bitbucket 
2. Development Installations 
3. Chapter Questions (Chs 1,2)
4. Bitbucket repo links: a) this assignment and b) the completed tutorials above
(bitbucketstationlocations and myteamquotes). 

> This is a blockquote. 
>
> This is the second paragraph in the blockquote. 
>
> #### Git commands w/short descriptions: 

1. gitinit- initialize your git repository locally 
2. git status- displays state of working directory and the staging area. 
3. git add- used when adding on to your respository online with changes
4. git commit- commits everything in staging to be ready to be pushed to remote repository. 
5. git push- pushing changes from local repository to remote repository. 
6. git pull- when you want to get your local repo up to date after remote changes made. 
7. git branch -a - lists all branches in working folder. 

#### Assignment Screenshots: 

*Screenshot of running java Hello*: 

![JDK Installation Screenshot](img/HelloWorld.JPG)

*Screenshot of running TomCat LocalHost*: 

![TomCat Installation Screenshot](img/Tomcat_Install.JPG)

#### Tutorial Links:

*Bitbucket Tutorial -Station Locations:*

[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/td16c/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/td16c/myteamquotes/ "My Team Quotes Tutorial")


