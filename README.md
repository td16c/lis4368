>**NOTE.** This README.md file should be *modified* and placed at the **root of *each* of your repos directories.** 

# LIS4368

## Tyler Dean 

### Assignment Requirements 

*Course Work Links:* 

1. [A1 README.md](a1/README.md "My A1 README.md file")
    
    *Install JDK*

    *Install Tomcat*

    *Provide screenshots of installations*

    *Create Bitbucket repo*

    *Complete Bitbucket Tutorials*

    *Bitbucketstationlocations and myteamquotes*

    *Provide git commands* 

2. [A2 README.md](a2/README.md "My A2 README.md file")
 
    *Install MySQL*

    *Develop and Deploy a Webapp*

    *Deploy Servlet using @WebServlet*

3. [A3 README.md](a3/README.md "My A3 README.md file")

    *ERD*

    *10 records of data*

    *docs folder a3.mwb a3.sql*

    *Blackboard links*     


4. [P1 README.md](p1/README.md "My P1 README.md file")    

    *JQuery Validation*

    *Regular Expressions*

    *Update Index.jsp* 

5. [A4 README.md](a4/README.md "My A4 README.md file")

    *Client side validation*
 
    *Failed validation*

    *Passed validation* 

6. [A5 README.md](a5/README.md "My A5 README.md file")
    
    *Passed validation* 
     
    *Data Entry*

    *User Entry*

7. [P2 README.md](p2/README.md "My P2 README.md file")

   *Passed validation* 
     
    *Data Entry*

    *User Entry*   
         