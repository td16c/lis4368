> **NOTE:** This README.md file should be placed at the **root of each 
of your repos directories.** 
>
>Also, this file **must** use Markdown syntax, and provide documentation
as per below--otherwise, points **will** be deducted.
>

#LIS4368 - Advanced Web Application Development 

## Tyler M. Dean 

### Assignment 2 Requirements: 

*Assignment Links* 

1. [A2 hello Link](http://localhost:9999/hello "hello")

2. [A2 HelloHome](http://localhost:9999/hello/HelloHome.html "HelloHome.html")

3. [A2 sayhello](http://localhost:9999/hello/sayhello "sayhello")

4. [A2 querybook](http://localhost:9999/hello/querybook.html "querybook")

5. [A2 sayhi](http://localhost:9999/hello/sayhi "sayhi")

*Screenshot of query results*: 

![Query Result](img/QueryResult.JPG)
