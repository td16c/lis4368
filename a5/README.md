# LIS4368

## Tyler Dean 

### Assignment 5 Requirements 

#### Deliveriables: 

 *Valid User Form Entry*
 *Passed validation screenshot* 
 *Associated Database Entry*
 *Blackboard Links:* 

*Assignment Screenshots* 

*Screenshot Valid User Form Entry*: 

![Valid User From](img/Valid.JPG "Valid User Form")

*Screenshot Passed Validation*:

![Passed validation](img/Passed.JPG "Passed validation")

*Screenshot Associated Database Entry*:

![Database Entry](img/Data.JPG "Data Entry")







